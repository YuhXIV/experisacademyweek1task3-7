﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class Task_6
    {
        public static void BMI()
        {
            string input;
            double bmi;
            double weight = 0;
            double height = 0;
            Console.WriteLine("Enter your weight");
            do
            {
                input = Console.ReadLine();
                if (!Numbers(input)) Console.WriteLine("Only number allowed");
                else weight = Double.Parse(input);

            } while (!Numbers(input));

            Console.WriteLine("Enter your height");
            do
            {
                input = Console.ReadLine();
                if (!Numbers(input)) Console.WriteLine("Only number allowed");
                else height = Double.Parse(input);

            } while (!Numbers(input));

            bmi = CalcBMI(weight, height);

            Console.WriteLine($"Your BMI is {bmi} and you are {BodyStatue(bmi)}");
        }

        static double CalcBMI(double weight, double heigth)
        {
            return weight/(heigth*heigth);
        }

        static string BodyStatue(double bmi)
        {
            if(bmi < 18.5)
            {
                return "Underweight";
            } else if (bmi>=18.5 && bmi <= 24.9)
            {
                return "Normal weight";
            } else if (bmi > 25 && bmi < 30)
            {
                return "Overweight";
            } else
            {
                return "Obese";
            }
        }
        
        static bool Numbers(string input)
        {
            char[] tmp = input.ToCharArray();
            if (tmp.Length != 0 && tmp[0] == '.') return false;
            int counter = 0;
            foreach(char c in tmp)
            {
                if ((int)c > 58)
                {
                    if ((int)c < 48)
                    {
                        if (c == '.')
                        {
                            counter++;
                            if (counter <= 2) return false;
                        }
                        else return false;
                    }else
                    {
                        return false;
                    }
                }   
            }
            return true;
        }
    }
}
