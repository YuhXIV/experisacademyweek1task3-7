﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class Task_4
    {
        public static void NestedRectangle()
        {
            string input;
            int row;
            int col;
            bool successParsed;
            int max = 50;
            int min = 7;
            do
            {
                do
                {
                    Console.WriteLine($"Please enter the number row of the rectangle(NxN) in integer(Min {min}, Max {max}): ");
                    input = Console.ReadLine();
                    successParsed = Int32.TryParse(input, out row);

                } while (!successParsed || row < min || row > max);

                do
                {
                    Console.WriteLine($"Please enter the number col of the rectangle(NxN) in integer(Min {min}, Max {max}): ");
                    input = Console.ReadLine();
                    successParsed = Int32.TryParse(input, out col);

                } while (!successParsed || col < min || col > max);
                if (row == col) Console.WriteLine("Not allowed to make a square");
            } while (row == col);
            CreateRectangle(row, col);
        }

        static void CreateRectangle(int row, int col) 
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (j == 0 || j == col - 1 || i == 0 || i == row - 1) Console.Write("# ");
                    else if (j == 2 && i != 1 && i != row - 2
                        || j == col - 3 && i != row - 2 && i != 1
                        || i == 2 && j != 1 && j != col - 2
                        || i == row - 3 && j != col - 2 && j != 1)
                        Console.Write("# ");
                    else Console.Write("  ");
                }
                Console.WriteLine(" ");
            }
        }
    }
}
