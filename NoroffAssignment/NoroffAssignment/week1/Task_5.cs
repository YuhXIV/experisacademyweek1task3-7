﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class Task_5
    {
        public static void NameSearch()
        {
            string[] names = { "Kjetil hel", "Kjell hel", "Orjan", "Markus", "Emil" };
            string input;
            Console.WriteLine("Enter the name you want to search: ");
            do
            {
                input = Console.ReadLine();
                if (!Letter(input)) Console.WriteLine("Only characters allowed!");

            } while (!Letter(input));

            List<string> matchingName = Search(input, names);
            if(matchingName.Count == 0)
            {
                Console.WriteLine("Found no match");
            } else
            {
                Console.WriteLine("Matching name:");
                foreach(string c in matchingName)
                {
                    Console.Write($"{c}, ");
                }
                
            }
        }

        static List<string> Search(string name, string[] nameArray)
        {
            List<string> matchingName = new List<string>();
            foreach (string c in nameArray)
            {
                
                if (c.ToLower().Contains(name.ToLower()))
                {
                    matchingName.Add(c);
                    continue;
                }
            }
            return matchingName;
        }

        static bool Letter(string name) // check for the first letter is uppercase
        {
            char[] tmp = name.ToCharArray();
            foreach (char c in tmp)
            {
                if (!((int)c > 64) && !((int)c < 91) || !((int)c > 96 && (int)c < 123))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
